
<!-- # Table of Contents -->

<!-- 1.  [CIPHER C PROJECT](#org8f6728e) -->
<!--     1.  [ABOUT](#org05bcb99) -->
<!--     2.  [HOW USER WILL USE IT?](#orgd6a6f2f) -->
<!--     3.  [TASKS LIST](#orgf939900) -->
<!--     4.  [CODE LAYOUT](#org28a1158) -->
<!--     5.  [Ashwin](#orgf6b243a) -->
<!--         1.  [Project Details:](#org7c90d0a) -->
<!--         2.  [Using Git](#org20c8f66) -->
<!--     6.  [TODOS](#org0bab880) -->

<a id="org05bcb99"></a>

# About

Cipher is a very basic text encryption program.

It has two high level function.

-   TO ENCRYPT
    
    INPUT         &#x2013;&#x2014;> Processing -&#x2014;> OUTPUT
    
    Text file(normal)  -&#x2014;> encryption -&#x2013;&#x2014;> text file (encrypted)

-   TO DECRYPT
    
    INPUT         &#x2013;&#x2014;> Processing -&#x2014;> OUTPUT
    
    Text file(encrypted)  -&#x2014;> decryption -&#x2013;&#x2014;> text file (normal)
    
    TLDR: We open text file do sth to each alphabet and get unreadable file (but here the catch we can undo this process!)


<a id="orgd6a6f2f"></a>

# How user will use it

It is command line program so a terminal is needed.

-   By giving our program a input file

		$ cipher input.txt

-   Now cipher will generate output.txt in the same folder

-   INTERACTIVE MODE!
    
        $ cipher --interactive
    
    Will launch a prompt 
    
         Hello and welcome to cipher!
        
        >> 
    
    You can type words into the prompt and it will get encrypted! 
    
        Hello and welcome to cipher!
        
        >> hello
        eadfj
        
        >> raju
        kdsj


<a id="orgf939900"></a>

# Tasks List

1 Writing welcome and help messages
Program launch garda ke dekhincha r kasari use garne information

2 Validation logic and error messages
Galat option r command diyo bhane error dini texts

3 Reading text file
Simple text file read garne function

4 Writing out text file
Simple text file read garne function

1.  Writing a logger setup

Nothing much simple function that writes messages to file when called.

6 Write Makefile
program compile and build logic

7 Parsing out the cli arguments to struct
User le deko command and option yeuta struct ma organize garne

8 Core encryption logic
Text r key file ko content lini r encrypted text nikalni function


<a id="org28a1158"></a>

# Code Layout

    // ... c headers
    
    
    int main(//......){
    	 // Recieve input as commands from terminal
    	 struct Command command = parse_user_input()
    
    
    	 //if no commands Greet the user and give him instructions
    	 greet_with_about_and_help()
    
    	 // if User says a wrong command give hime usage errors
    	 validate_input_struct()
    
    	 // Write our own key array
    	 int *keys = {};
    
    	 // Read text file
    	 get_encrypted_text(file)
    
    	 // ENcrypt 
    	 get_encrypted_text(text, key)
    
    	 // write out the encrypted text
    	 get_encrypted_text(text, key)
    
    
    	 // if use says so launch interactive mode
    	 interactive_propmt_loop()
    }


<a id="orgf6b243a"></a>

# Ashwin

This is a page for the project given to ascol students of C subject in 2021.


<a id="org7c90d0a"></a>

## Project Details:

The project is about creating a simple [Data Encryption Standard(DES)](https://en.wikipedia.org/wiki/Data_Encryption_Standard).
To be a little familiar with DES, you may want to learn about SDES from:

-   (pdf) <http://mercury.webster.edu/aleshunas/COSC%205130/G-SDES.pdf>
-   (pdf) <http://ict.siit.tu.ac.th/~steven/css322y11s2/unprotected/CSS322Y11S2H01-DES-Examples.pdf>
-   (implementation) <https://toribeiro.com/simplified-DES-implementation-in-C/>

The Breaking down of project has not yet been done.
This is to be done after feedback from my fellow peers.


<a id="org20c8f66"></a>

## Using Git

This project is managed using git. The primary location of git host is:
<https://codeberg.org/boink/c-project/>

1.  setting up the worktree

    setting up the worktree for git is easy. just type these commands in
    either a terminal emulator  or cmd.exe
    
        git config --global user.name "Name Here"
        git config --global user.email "email@domain.tld"
        git clone 'https://codeberg.org/boink/c-project/'
        cd c-project
        git checkout -b mybranch 
    
    These will setup the name, email and then prepare a new branch for you to work on.
    Please note that working in a new branch is highly recommended instead of using the master branch directly.

2.  Commiting

    If you did the above steps, you're on a new branch called `mybranch`.
       Do note that you should commit in a sense of completion of small tasks. this makes it easy to revert to certain period of history.
       After working, commit your changes with:
    
        git add "file you worked on" "file2" "file3" "etc"
        git commit 
    
    This will ask you for a commit message and a commit body. Put that as asked.

3.  Merging with upstream (Hasta la vista "pull request")

    After you've worked on your branch, it'd be nice if we could also look at it.
    To do that, simply prepare a patch and send an email containing the said patch.
    These commands create a patch which you can attach as a email attachment to
    ashwin (the at symbol) ashwink.com.np
    After sending the patch, I'll update that on the server but in your seperate branch.
    You can click on the branch link in project homepage to see your branch and commits that you've sent.
    
    You can use this commands to make .patch files which are to be attached in the email body.
    
        git format-patch origin
    
    Do note that git send-email is a lot better but requires some teeny tiny configuration.
    
    1.  Unrelated note (skip to next part)
    
        The reason for using email is because git was *made* to be used with email.
        Please note that terrible clients like \(gmail\) are terrible and for better email experience,
        you should think about using something like Thunderbird.
        For someone informed opinion: <https://drewdevault.com/2018/07/02/Email-driven-git.html>


<a id="org0bab880"></a>

# Todos

-   Refactor messages to a Message struct in messages.h and have types as in struct
-   Refactor parsing and validation to another parser.h

