.. Cipher-Project documentation master file, created by
   sphinx-quickstart on Sat Sep  4 23:10:30 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Cipher-Project's documentation!
==========================================

.. toctree::
   cipher.md
   :maxdepth: 2
   :caption: Contents:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
